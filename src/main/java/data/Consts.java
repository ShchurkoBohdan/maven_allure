package data;

public class Consts {
    public static final String EMAIL = "shchurko.bohdan";
    public static final String PASSW = "Qwerty_123!";
    public static final String PATH_TO_PROPERTIES_FILE = "src/main/resources/config.properties";
    public static final String PATH_TO_RECIPIENTS_EMAILS_FILE = "src/test/resources/testEmails.csv";
    public static final String PATH_TO_USERS_FILE = "src/test/resources/users.csv";
    public static final int COUNT_OF_EMAILS = 3;

    private Consts(){}
}
